package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new"rockpaperscissors -object with the
    	 * code `new"rockpaperscissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String humanChoice;
    String continuePlaying;
    


    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);

            while (true) {
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (humanChoice.equals("rock")|| humanChoice.equals("paper")|| humanChoice.equals("scissors")){
                    break;
                }
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                continue;
            }
            
            Random random = new Random();
            int randomNumber = random.nextInt(3);

            String computerChoice;
                if (randomNumber == 0) {
                    computerChoice = "rock";
                } else if (randomNumber == 1) {
                    computerChoice = "paper";
                } else {
                    computerChoice = "scissors";
                }
                
        
            
            if (humanChoice.equals(computerChoice)){
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". It's a tie!");
            }

            else if(humanChoice.equals("rock")){
                if (computerChoice.equals("paper")) {
                    computerScore = computerScore + 1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                }

                else {
                    humanScore = humanScore +1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                }
            }

            else if (humanChoice.equals("paper")){
                if (computerChoice.equals("scissors")) {
                    computerScore = computerScore + 1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                }
                else {
                    humanScore = humanScore + 1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                }

            }

            else if (humanChoice.equals("scissors")){
                if (computerChoice.equals("rock")){
                    computerScore = computerScore + 1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                }
                else {
                    humanScore = humanScore + 1;
                    System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                }
            
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            continuePlaying = readInput("Do you wish to continue playing? (y/n)?");         
            if (continuePlaying.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter = roundCounter + 1;
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
